<?php
    date_default_timezone_set("Asia/Jakarta");
    
    echo date('d-m-Y H:i:s');
    echo "<br>";
    
    ini_set('max_execution_time', 0);
    set_time_limit(0);

    require_once('./../wablas/wablas.php');
    require_once('./../config/koneksi.php');
    require_once('./../config/database.php');

    $wablas = new Wablas();
    $mysqli = new Database($host, $user, $pass, $database);
    $db = $mysqli->conn;

    $sql = "SELECT * FROM tbl_notifikasi WHERE whatsapp_status = 'pending' OR whatsapp_status = 'gagal' LIMIT 150";
    $query = $db->query($sql) or ($db->error);
    if ($query->num_rows !== 0) {
        while($data = $query->fetch_object()) {
            $recipients = [];

            $sql = "SELECT * FROM tbl_pegawai WHERE NIP = '$data->penerima'";
            $queryUser = $db->query($sql) or ($db->error);
            if ($queryUser->num_rows !== 0) {
                $dataUser = $queryUser->fetch_object();
                $recipients = [$dataUser->TELEPON];
            }

            $message = $data->pesan;
    
            $resp = $wablas->sendMessage($recipients, $message);
            if ($resp->status === 1 || $resp->status === true) {
                $status = "sukses";
            } else {
                if ($data->whatsapp_status === 'gagal') {
                    $status = "batal";
                } else {
                    $status = "gagal";
                }
            }
            $detail_status = json_encode($resp);
            $sql = "UPDATE tbl_notifikasi SET whatsapp_status = '$status', whatsapp_detail = '$detail_status' WHERE id = $data->id";
            $update = $db->query($sql) or ($db->error);
            echo "<pre>";
            print_r(json_encode($resp, JSON_PRETTY_PRINT));
        }
    }
    echo "<br>";
?>