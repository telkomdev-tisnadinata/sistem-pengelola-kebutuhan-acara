<?php
include "models/m_pengajuan.php";
$pgj = new Pengajuan($connection);
?>
<script>
    function printDiv(id) {
        $.get( "views/pengajuan_detail.php?id=" + id, function( data ) {
            var mywindow = window.open('', 'new div', "width="+screen.availWidth+",height="+screen.availHeight);
            mywindow.document.write(data);
            mywindow.focus();
            mywindow.print();
            mywindow.close();

            return true;
        });
    }
</script>
<div class="row">
          <div class="col-lg-12">
            <h1>Lihat Pengajuan <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.php"><i class="icon-dashboard"></i> Lihat Peminjaman</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Nama Pengaju</th>
                            <th>NIP</th>
                            <th>Divisi Kerja</th>
                            <th>Data Barang</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                        <?php
                        $no = 1;
                        $tampil = $pgj->tampil();
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                                $actual_link = explode('?',$link)[0];
                                $url = $actual_link.'views/pengajuan_detail.php&id='.$data->id;            
                        ?>
                            <form action="" method="post">
                                <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                                <tr>
                                    <td align="center"><?php echo $no++ ?></td>
                                    <td><?php echo $data->nama_pengaju; ?></td>
                                    <td><?php echo $data->nip; ?></td>
                                    <td><?php echo $data->divisi_kerja; ?></td>
                                    <td><?php echo $data->jenis_barang.' - '.$data->nama_barang.' ('.$data->kd_barang.')'; ?></td>
                                    <td><?php echo $data->keterangan; ?></td>
                                    <td><a href="#" onClick="printDiv(<?php echo $data->id; ?>)" class="btn btn-info btn-xs" >LIHAT DETAIL</a></td>
                                </tr>
                            </form>
                        <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>