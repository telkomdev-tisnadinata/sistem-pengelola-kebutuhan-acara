<?php
include "models/m_barang.php";
if (isset($_POST['ubah'])) {
    header("Location: ./?page=barang_edit&kd_barang=".$_POST['kd_barang']);
    die();
}
$brg = new Barang($connection);
?>
<style type="text/css" media="print">
  @page { size: landscape; }
</style>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var mywindow = window.open('', 'new div', "width="+screen.availWidth+",height="+screen.availHeight);
        mywindow.document.write('<html><head><title></title>');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/sb-admin.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(printContents);
        mywindow.document.write('</body></html>');
        mywindow.focus();
        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
<div class="row">
    <div class="col-lg-12">
    <h1>Lihat Barang <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="icon-dashboard"></i> Lihat Barang</a></li>
        <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
    </ol>
    </div>
</div><!-- /.row -->

        <div class="">
            <div class="col-lg-12">
            <?php
                if (isset($_POST['hapus'])) {
                    $hapus = $brg->hapus($_POST['kd_barang']);
                    $alert = 'alert alert-success';
                    $message = '<strong>Success!</strong> Data Berhasil Dihapus.';
                    if (!$hapus) {
                    $alert = 'alert alert-danger';
                    $message = '<strong>Fail!</strong> Gagal Menghapus Data.';
                    }
                    echo "
                    <div class='".$alert."'>
                        ".$message."
                    </div>
                    ";
                }
            ?>
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Jenis Barang</th>
                            <th>Spesifikasi Barang</th>
                            <th>Tanggal Pengadaan</th>
                            <th></th>
                        </tr>
                        <?php
                        $no = 1;
                        $tampil = $brg->tampil();
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                        ?>
                            <form action="" method="post">
                                <input type="hidden" name="kd_barang" value="<?php echo $data->kd_barang; ?>"/>
                                <tr>
                                    <td align="center"><?php echo $no++ ?></td>
                                    <td><?php echo $data->kd_barang; ?></td>
                                    <td><?php echo $data->nama_barang; ?></td>
                                    <td><?php echo $data->jenis_barang; ?></td>
                                    <td><?php echo $data->spesifikasi_barang; ?></td>
                                    <td><?php echo $data->tanggal_pengadaan; ?></td>
                                    <td>
                                    <input type="submit" class="btn btn-warning btn-xs" name="ubah" value="UBAH"/>
                                    <input type="submit" class='btn btn-danger btn-xs' name="hapus" value="HAPUS" onclick="return confirm('Apakah anda yakin?');"/>
                                    
                                    <a class="btn btn-info btn-xs" data-toggle="modal" data-id="<?php echo $data->kd_barang; ?>" href="#myModal" id="kd_barang" >RIWAYAT PINJAM</a>
                                   
                                    <div id="confirm" class="modal hide fade">
                                        <div class="modal-body">
                                            Are you sure?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                    
                                    <div id="myModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog modal-lg" style="width: 80%">
                                                <!-- konten modal-->
                                                <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Riwayat Barang</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body" id="myModalPrint" style="overflow-x: auto;max-width: 95%;">
                                                        <div class="fetched-data"></div>
                                                    </div>
                                                    <!-- footer modal -->
                                                    <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" onClick="printDiv('myModalPrint');">Print</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </td>
                                </tr>
                            </form>
                        <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script>
        $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            $.ajax({
                type : 'get',
                url : 'views/peminjaman_riwayat.php?kd_barang=' + rowid , //Here you will fetch records 
                success : function(data){
                $('.fetched-data').html(data);//Show fetched data from database
                }
            });
        });
        
    });
    </script>