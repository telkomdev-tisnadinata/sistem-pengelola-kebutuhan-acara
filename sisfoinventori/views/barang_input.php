<div class="row">
          <div class="col-lg-12">
            <h1>Input Barang <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.php"><i class="icon-dashboard"></i> Input Barang</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        <?php
          include "models/m_barang.php";
          $brg = new Barang($connection);
        
          if (isset($_POST['tambah'])) {
            $tambah = $brg->tambah($_POST);
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Data Berhasil Ditambahkan.';
            if (!$tambah) {
              $alert = 'alert alert-danger';
              $message = '<strong>Fail!</strong> Gagal Menambahkan Data.';
            }
            echo "
              <div class='".$alert."'>
                ".$message."
              </div>
            ";
          }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label class="control-label" for="kd_barang">Kode Barang</label>
                <input type="text" name="kd_barang" class="form-control" id="kd_barang" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="nama_barang">Nama Barang</label>
                <input type="text" name="nama_barang" class="form-control" id="nama_barang" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="jenis_barang">Jenis Barang</label>
                <input type="text" name="jenis_barang" class="form-control" id="jenis_barang" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="spesifikasi_barang">Spesifikasi Barang</label>
                <textarea class="form-control" rows="5" id="spesifikasi_barang" name="spesifikasi_barang"></textarea>
              </div>
              <div class="form-group">
                <label class="control-label" for="tanggal_pengadaan">Tanggal pengadaan</label>
                <input type="date" name="tanggal_pengadaan" class="form-control" id="tanggal_pengadaan" required>
              </div>
            </div>
            <!-- Button simpan -->
            <div id="tambah" class="modal-footer">
              <input type="submit" class="btn btn-success" name="tambah" value="TAMBAH">
            </div>
            </div>
        </form>

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>
