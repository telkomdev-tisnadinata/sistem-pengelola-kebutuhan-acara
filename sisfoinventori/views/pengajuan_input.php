<div class="row">
          <div class="col-lg-12">
            <h1>Input Pengajuan <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.php"><i class="icon-dashboard"></i> Input Peminjaman</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        <?php
          include "models/m_barang.php";
          include "models/m_pengajuan.php";
          $pgj = new Pengajuan($connection);
          $brg = new Barang($connection);
        
          if (isset($_POST['tambah'])) {
            $tambah = $pgj->tambah($_POST);
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Data Berhasil Ditambahkan.';
            if (!$tambah) {
              $alert = 'alert alert-danger';
              $message = '<strong>Fail!</strong> Gagal Menambahkan Data.';
            }
            echo "
              <div class='".$alert."'>
                ".$message."
              </div>
            ";
          }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label class="control-label" for="nama_pengaju">Nama Pengaju</label>
                <input type="number" name="nama_pengaju" class="form-control" id="nama_pengaju" placeholder="Nama Pengaju" value="<?php echo $_SESSION['login_user']->NAMA ?>" readonly>
              </div>
              <div class="form-group">
                <label class="control-label" for="nip">NIP Pengaju</label>
                <input type="number" name="nip" class="form-control" id="nip" placeholder="NIP Pengaju" value="<?php echo $_SESSION['login_user']->NIP ?>" readonly>
              </div>
              <div class="form-group">
                <label>Divisi Kerja</label>
                <select class="form-control" name="divisi_kerja">
                  <option value="Bagian Teknik">Bagian Teknik</option>
                  <option value="Bagian Program">Bagian Program</option>
                  <option value="Bagian Berita">Bagian Berita</option>
                </select>
              </div>
              <div class="form-group">
                <label for="barang">Pilih Barang</label>
                <select class="form-control" id="barang" name="barang">
                  <?php
                    $no = 1;
                    $tampil = $brg->tampil();
                    if (!$tampil) {
                    ?>
                      <option>Tidak Dapat Mengambil Data Barang</option>
                    <?php
                    } else {
                      while($data = $tampil->fetch_assoc()){
                  ?>
                        <option value="<?php echo $data['kd_barang']; ?>">
                          <?php echo $data['jenis_barang'].' - '.$data['nama_barang'].' ('.$data['kd_barang'].')'; ?>
                        </option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="keterangan">Keterangan</label>
                <textarea class="form-control" rows="5" id="keterangan" name="keterangan" placeholder="Keterangan Pengajuan" Required></textarea>
              </div>
            </div>
            <!-- Button simpan -->
            <div id="tambah" class="modal-footer">
              <input type="submit" class="btn btn-success" name="tambah" value="TAMBAH">
            </div>
            </div>
        </form>

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>
