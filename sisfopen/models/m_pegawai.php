<?php
class Pegawai {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    // MENGHITUNG JUMLAH DATA YANG ADA
    public function jumlah_pegawai(){
        $db = $this->mysqli->conn;
        $sql = "SELECT count(*) as jumlah FROM tbl_pegawai";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN NIP
    public function tampil($NIP = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_pegawai";
        if($NIP != null){
            $sql .= " WHERE NIP = '$NIP'";
        }
        $sql .= " ORDER BY NIP ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN KOLOM TERTENTU
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_pegawai";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY NAMA ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENAMBAHKAN DATA  BARU KE DATABASE
    public function tambah($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO tbl_pegawai(NIP,NAMA,JABATAN,EMAIL,TELEPON,PASSWORD)";
        $sql .= "VALUES('".$data['NIP']."','".$data['NAMA']."','".strtoupper($data['JABATAN'])."','".$data['EMAIL']."','".$data['TELEPON']."','".$data['PASSWORD']."')";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGHAPUS DATA DARI DATABASE BERDASARKAN NIP
    public function hapus($NIP){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_pegawai WHERE NIP = '$NIP'";
        if($NIP == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    // MENGUBAH DATA DARI DATABASE BERDASARKAN NIP
    public function ubah($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_pegawai SET NAMA = '".$data['NAMA']."', JABATAN = '".$data['JABATAN']."', TELEPON = '".$data['TELEPON']."', ";
        $sql .= "EMAIL = '".$data['EMAIL']."', PASSWORD = '".$data['PASSWORD']."' WHERE NIP = '".$data['NIP']."'";
        if($data['NIP'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    // MENJALANKAN SQL QUERY BEBAS
    public function query($sql){
        $db = $this->mysqli->conn;
        if($sql == null) {
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    // MENGAMBIL STATUS PENUGASAN PEGAWAI APAKAH SEDANG BERTUGAS ATAU TIDAK
    public function status_penugasan($NIP, $TANGGAL){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_penugasan p, tbl_jadwal j WHERE p.NIP='".$NIP."' ";
        $sql .= "AND ((('".$TANGGAL['MULAI']."' BETWEEN p.TANGGAL_MULAI AND p.TANGGAL_SELESAI) OR ('".$TANGGAL['SELESAI']."' BETWEEN p.TANGGAL_MULAI AND p.TANGGAL_SELESAI)) ";
        $sql .= "OR ((p.TANGGAL_MULAI BETWEEN '".$TANGGAL['MULAI']."' AND '".$TANGGAL['SELESAI']."') OR (p.TANGGAL_SELESAI BETWEEN '".$TANGGAL['MULAI']."' AND '".$TANGGAL['SELESAI']."'))) ";
        $sql .= "AND (p.JAM = '".$TANGGAL['JAM']."' OR p.JAM = 'Tidak dalam shift') AND p.KODE_JADWAL = j.KODE_JADWAL";
        if($sql == null) {
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
}
?>