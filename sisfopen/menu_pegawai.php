<!-- HALAMAN UNTUK MEMBUAT MENU DI PEGAWAI -->
<ul id="sidebarnav">
    
    <li> <a class="waves-effect waves-dark" href="?page=lihat_jadwal_saya" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Lihat Tugas Saya</span></a>
    </li>
    <li> <a class="waves-effect waves-dark" href="?page=penugasan_upload" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Upload Bukti Tugas</span></a>
    </li>
    <li> <a class="waves-effect waves-dark" href="?page=profile_pegawai" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Profile Saya</span></a>
    </li>
    <li> <a class="waves-effect waves-dark" href="auth/logout.php" aria-expanded="false"><i class="mdi mdi-power"></i><span class="hide-menu">SIGN OUT</span></a>
    </li>
</ul>
