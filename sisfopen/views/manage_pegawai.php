<?php
    include "models/m_pegawai.php";
    $pgw = new Pegawai($connection);
?>

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <?php
                    if (isset($_GET['hapus']) || isset($_POST['ubah']) || isset($_POST['tambah'])) {
                        if (isset($_GET['hapus'])) {
                            $data = $pgw->hapus($_GET['hapus']);
                            $message = 'Dihapus';
                        } else if (isset($_POST['ubah'])) {
                            $data = $pgw->ubah($_POST);
                            $message = 'Diubah';
                        } else if (isset($_POST['tambah'])) {
                            $data = $pgw->tambah($_POST);
                            $message = 'Ditambahkan';
                        }
                        if ($data) {
                            echo '
                            <div class="alert alert-success"> <i class="ti-user"></i> Data Berhasil '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else {
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Data Gagal '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        }
                    }
                    $NIP = '';
                    $NAMA = '';
                    $JABATAN = '';
                    $EMAIL = '';
                    $TELEPON = '';
                    $PASSWORD = '';
                    if (isset($_GET['ubah'])) {
                        $data = $pgw->tampil_filter('NIP', $_GET['ubah']);
                        $data = $data->fetch_object();
                        $NIP = $data->NIP;
                        $NAMA = $data->NAMA;
                        $JABATAN = $data->JABATAN;
                        $TELEPON = $data->TELEPON;
                        $EMAIL = $data->EMAIL;
                        $PASSWORD = $data->PASSWORD;
                    }
                ?>
                <form action="" class="form-material" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nip" class="col-md-12">NIP <small></small></label>
                                <div class="col-md-12">
                                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" name="NIP" value="<?php echo $NIP;?>" <?php echo (isset($_GET['ubah'])) ? 'readonly' : 'required';?>>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nama" class="col-md-12">Nama</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="NAMA" id="nama" value="<?php echo $NAMA;?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">Jabatan</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="JABATAN" name="JABATAN">
                                        <option value="PRODUSER" <?php echo ($JABATAN == 'PRODUSER') ? 'selected' : '' ?>>PRODUSER</option>
                                        <option value="ATASAN" <?php echo ($JABATAN == 'ATASAN') ? 'selected' : '' ?>>ATASAN</option>
                                        <option value="KAMERAWAN" <?php echo ($JABATAN == 'KAMERAWAN') ? 'selected' : '' ?>>KAMERAWAN</option>
                                        <option value="PENGARAH ACARA" <?php echo ($JABATAN == 'PENGARAH ACARA') ? 'selected' : '' ?>>PENGARAH ACARA</option>
                                        <option value="REPORTER" <?php echo ($JABATAN == 'REPORTER') ? 'selected' : '' ?>>REPORTER</option>
                                        <option value="EDITOR" <?php echo ($JABATAN == 'EDITOR') ? 'selected' : '' ?>>EDITOR</option>
                                        <option value="UNIT MANAGER" <?php echo ($JABATAN == 'UNIT MANAGER') ? 'selected' : '' ?>>UNIT MANAGER</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email" class="col-md-6">Telepon</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="TELEPON" value="<?php echo $TELEPON;?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email" class="col-md-6">Email</label>
                                <div class="col-md-12">
                                    <input type="email" class="form-control" name="EMAIL" value="<?php echo $EMAIL;?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12">Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" name="PASSWORD" value="<?php echo $PASSWORD;?>" required>
                                </div>
                            </div>
                        </div>                    
                    </div>                    
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php
                                if (isset($_GET['ubah'])) {
                                    echo '<input type="submit" class="btn btn-success" name="ubah" value="UBAH & SIMPAN"/>';
                                } else {
                                    echo '<input type="submit" class="btn btn-success" name="tambah" value="TAMBAH"/>';
                                }
                            ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Daftar Pegawai TVRI Jawa Barat Bidang Berita</h4>
                <div class="table-responsive">
                    <table class="table" style="overflow-y:scroll;max-height:750px;display:block;">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NIP</th>
                                <th>NAMA PEGAWAI</th>
                                <th>EMAIL</th>
                                <th>TELEPON</th>
                                <th>JABATAN</th>
                                <th>STATUS PENUGASAN</th>
                                <th>KETERANGAN</th>                        
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $tampil = $pgw->tampil();
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                date_default_timezone_set('Asia/Jakarta');
                                $time = strtotime(date('H:i:s'));
                                $TANGGAL = array(
                                    'MULAI' => date("Y-m-d"),
                                    'SELESAI' => date("Y-m-d"),
                                    'JAM' => 'Tidak dalam shift'
                                );
                                if (strtotime('08:00:00') <= $time && $time <= strtotime('12:00:00')) {
                                    $TANGGAL['JAM'] = 'Shift 1 ( Pukul 08:00 - 12:00 )';
                                } else if (strtotime('13:00:00') <= $time && $time <= strtotime('17:00:00')) {
                                    $TANGGAL['JAM'] = 'Shift 2 ( Pukul 13:00 - 17:00 )';
                                } else if (strtotime('18:00:00') <= $time && $time <= strtotime('21:00:00')) {
                                    $TANGGAL['JAM'] = 'Shift 3 ( Pukul 18:00 - 21:00 )';
                                }
                                $checkDuty = $pgw->status_penugasan($data->NIP, $TANGGAL);
                                $onDuty = $checkDuty->num_rows;
                                $checkDuty = $checkDuty->fetch_object();
                                $status_penugasan = ($onDuty != 0) ? 'DALAM TUGAS ('.$checkDuty->NAMA_ACARA.')' : 'TIDAK DALAM PENUGASAN';
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NIP; ?></td>
                                <td><?php echo $data->NAMA; ?></td>
                                <td><?php echo $data->EMAIL; ?></td>
                                <td><?php echo $data->TELEPON; ?></td>
                                <td><?php echo $data->JABATAN; ?></td>
                                <td align="center"><strong><?php echo $status_penugasan; ?></strong></td>
                                <td>
                                    <a href="?page=<?php echo $_GET['page'].'&ubah='.$data->NIP;?>"> <button type="button" class="btn waves-effect waves-light btn-warning btn-xs">UBAH</button>
                                    <a href="?page=<?php echo $_GET['page'].'&hapus='.$data->NIP;?>" onclick="return konfirmasiHapus('<?php echo $data->NAMA; ?>');"> <button type="button" class="btn waves-effect waves-light btn-danger btn-xs">HAPUS</button>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->