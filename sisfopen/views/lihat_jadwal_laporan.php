<?php
    include "models/m_jadwal.php";
    $jwl = new Jadwal($connection);
?>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Daftar Acara Yang Sedang Berjalan</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nama Acara</th>
                                <th>Produser Acara</th>
                                <th>Jumlah Karyawan</th>
                                <th>Lokasi</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Jam</th>
                                <th> </th>
                        
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $tampil = $jwl->tampil_jadwal_by_status("STATUS = 'RUNNING'");
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                $jumlah_karyawan = $jwl->query('SELECT NIP FROM tbl_penugasan WHERE KODE_JADWAL = '.$data->KODE_JADWAL);
                                $jumlah_karyawan = $jumlah_karyawan->num_rows;
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $data->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $jumlah_karyawan; ?> orang</td>
                                <td><?php echo $data->LOKASI; ?></td>
                                <td><?php echo $data->TANGGAL_MULAI; ?></td>
                                <td><?php echo $data->TANGGAL_SELESAI; ?></td>
                                <td><?php echo $data->JAM; ?></td>
                                <td>
                                    <a href="?page=lihat_jadwal_detail<?php echo '&detail='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-info btn-xs">ON PROGRESS</button></a>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Daftar Acara Yang Telah Selesai</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nama Acara</th>
                                <th>Produser Acara</th>
                                <th>Jumlah Karyawan</th>
                                <th>Lokasi</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Jam</th>
                                <th> </th>
                        
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $tampil = $jwl->tampil_jadwal_by_status("STATUS = 'FINISHED'");
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                $jumlah_karyawan = $jwl->query('SELECT NIP FROM tbl_penugasan WHERE KODE_JADWAL = '.$data->KODE_JADWAL);
                                $jumlah_karyawan = $jumlah_karyawan->num_rows;
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $data->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $jumlah_karyawan; ?> orang</td>
                                <td><?php echo $data->LOKASI; ?></td>
                                <td><?php echo $data->TANGGAL_MULAI; ?></td>
                                <td><?php echo $data->TANGGAL_SELESAI; ?></td>
                                <td><?php echo $data->JAM; ?></td>
                                <td>
                                    <a href="?page=lihat_jadwal_detail<?php echo '&detail='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-success btn-xs">SELESAI</button></a>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->