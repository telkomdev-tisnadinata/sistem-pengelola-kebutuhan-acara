FROM telkomindonesia/alpine:php-5.6-apache-novol
MAINTAINER Dimas Restu Hidayanto <dimas@playcourt.id>

# Set Working Directory Under Repository Directory
WORKDIR /var/www/data/html

# Copy all file inside repository to Working Directory
COPY . .
